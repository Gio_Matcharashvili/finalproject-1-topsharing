package com.example.topsharing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_profile.*
import java.util.HashMap


class ProfileActivity : AppCompatActivity() {

    private val posts = arrayListOf<String>()
    private val usernames = arrayListOf<String>()
    private val already_posted = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        init()
    }

    private fun init(){
        val username = intent.getStringExtra("username").toString()
        val avatar = intent.getStringExtra("avatar").toString()

        configure(username,avatar)

        profileNewsFeedButton.setOnClickListener{
            val newsFeedIntent = Intent(this, NewsFeedActivity::class.java)
            newsFeedIntent.putExtra("username",username)
            newsFeedIntent.putExtra("avatar",avatar)
            startActivity(newsFeedIntent)
        }
        setData(username)
        ProfilePostsRecyclerView.layoutManager = LinearLayoutManager(this)
        ProfilePostsRecyclerView.adapter = RecyclerViewAdapter(posts, usernames)

    }

    private fun configure(username:String,avatar:String){
        profileUsernameTextView.text = username
        if (avatar=="rock_icon"){
            profileAvatarImageView.setImageResource(R.drawable.rock_icon)
        }else if (avatar == "purple_head"){
            profileAvatarImageView.setImageResource(R.drawable.purple_head)
        }
    }

    private fun setData(username: String) {
        val database = Firebase.database
        val myRef = database.getReference("posts")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var base: HashMap<String, HashMap<String, String>>? = dataSnapshot
                    .getValue<HashMap<String, HashMap<String, String>>>()

                // keys are post ids.
                val keys = base?.keys

                for (key in keys!!) {
                    val inPostUsername = base?.get(key)?.get("username")

                    if (inPostUsername == username){
                        if (key !in already_posted){
                            val post = base?.get(key)?.get("post")
                            posts.add(post!!)
                            usernames.add(inPostUsername!!)
                            already_posted.add(key!!)
                        }

                    }

                }

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Toast.makeText(
                    baseContext, "Can't Connect to the server.",
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }

}