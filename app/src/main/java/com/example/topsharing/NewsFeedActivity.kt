package com.example.topsharing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_news_feed.*
import java.util.HashMap

class NewsFeedActivity : AppCompatActivity() {

    private val posts = arrayListOf<String>()
    private val usernames = arrayListOf<String>()
    private val alredy_posted= arrayListOf<String>()
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_feed)

        val username = intent.getStringExtra("username").toString()
        val avatar = intent.getStringExtra("avatar").toString()



        init()

    }
    private fun init(){

        database = Firebase.database.reference

        val username = intent.getStringExtra("username").toString()
        val avatar = intent.getStringExtra("avatar").toString()

        configure(username,avatar)

        newsAvatarImageButton.setOnClickListener{
            val profileIntent = Intent(this, ProfileActivity::class.java)
            profileIntent.putExtra("username",username)
            profileIntent.putExtra("avatar",avatar)
            startActivity(profileIntent)
        }

        newsPostAddButton.setOnClickListener{
            pushPost(username,avatar)
        }

        setData()
        newsPostsRecyclerView.layoutManager = LinearLayoutManager(this)
        newsPostsRecyclerView.adapter = RecyclerViewAdapter(posts,usernames)


    }

    private fun configure(username:String,avatar:String){
        newsUsernameTextView.text = username

        newsPostEditText.hint = "What's on your mind,$username?"

        if (avatar=="rock_icon") {
            newsAvatarImageButton.setImageResource(R.drawable.rock_icon)
        }


        else if (avatar == "purple_head"){
            newsAvatarImageButton.setImageResource(R.drawable.purple_head)
        }
    }

    private fun pushPost(username:String,avatar: String){
        val newPost = newsPostEditText.text.toString()

        val database = Firebase.database
        val myRef = database.getReference("posts")

        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var postHash = hashMapOf<String,String>()
                postHash.put("avatar",avatar)
                postHash.put("post",newPost)
                postHash.put("tags","none")
                postHash.put("username",username)

                val key = myRef.push().key
                if (key != null) {
                    val childUpdates = hashMapOf<String, Any>(key to postHash)
                    myRef.updateChildren(childUpdates)
                }
            }
            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Toast.makeText(baseContext, "Can't Connect to the server.",
                    Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setData(){
        val database = Firebase.database
        val myRef = database.getReference("posts")

        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var base: HashMap<String, HashMap<String, String>>? = dataSnapshot
                    .getValue<HashMap<String, HashMap<String, String>>>()

                // keys are postIDs
                val keys = base?.keys

                for(key in keys!!){
                    if (key !in alredy_posted){
                        val post = base?.get(key)?.get("post")
                        val inpostusername = base?.get(key)?.get("username")
                        posts.add(post!!)
                        usernames.add(inpostusername!!)
                        alredy_posted.add(key!!)
                    }

                }

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Toast.makeText(baseContext, "Can't Connect to the server.",
                    Toast.LENGTH_SHORT).show()
            }

        })
    }
}

