package com.example.topsharing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity() {

    // They are for Firebase Auth and database.
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        // Call the main function.
        init()
    }

    // Create main function.
    private fun init(){
        // This is for Firebase Auth.
        auth = Firebase.auth
        database = Firebase.database.reference

        // Set onclick listener to the inSignin Button.
        inSigninButton.setOnClickListener{
            // Call the SignIn function.
            SignIn()
        }
    }

    // Create SignIn function.
    private fun SignIn(){

        // Take all the credentials.
        val email = inEmailEditText.text.toString()
        val password = inPasswordEditText.text.toString()

        // Check if all the fields are filled, if it is not show error.
        if (email.isNotEmpty() && password.isNotEmpty()){

            // Connect firebase realtime database.
            val database = Firebase.database
            val myRef = database.getReference("users")

            // Make progressbar visible while user waits.
            inProgressBar.visibility = View.VISIBLE

            // Make inSignin Button not clicable while user waits.
            inSigninButton.isClickable = false

            // Starts sign in to the firebase.
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Toast.makeText(
                            baseContext, "Authentication succeed!",
                            Toast.LENGTH_SHORT
                        ).show()
                        val user = auth.currentUser

                        // Take information only once.
                        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                // This method is called once with the initial value and again
                                // whenever data at this location is updated.

                                // In the base we have all the user information
                                var base: HashMap<String, HashMap<String, String>>? = dataSnapshot
                                    .getValue<HashMap<String, HashMap<String, String>>>()

                                // keys are usernames
                                val keys = base?.keys


                                // Find user's email and avatar.
                                for (key in keys!!) {
                                    val checkEmail = base?.get(key)?.get("email")
                                    val avatar = base?.get(key)?.get("avatar")
                                    if (email == checkEmail){
                                        // If everything is succeed go to the home page.
                                        val profileIntent = Intent(baseContext, ProfileActivity::class.java)

                                        // Send information of the username and avatar.
                                        profileIntent.putExtra("username",key)
                                        profileIntent.putExtra("avatar",avatar)
                                        startActivity(profileIntent)

                                    }
                                }

                            }

                            override fun onCancelled(error: DatabaseError) {
                                // Failed to read value
                                Toast.makeText(
                                    baseContext, "Can't Connect to the server.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        })
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                    // After that make progressbar gone.
                    inProgressBar.visibility = View.GONE

                    // After wait make inSignin button clickable again.
                    inSigninButton.isClickable = true
                }



        }else{
            Toast.makeText(this, "Please fill all fields.", Toast.LENGTH_SHORT).show()
        }
    }

}