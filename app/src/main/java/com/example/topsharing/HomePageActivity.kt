package com.example.topsharing

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class HomePageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Call the main function.
        init()
    }

    // Main function which is private.
    private fun init(){

        // Add onClickListener to the login Button at the homepage.
        homeLoginButton.setOnClickListener{

            // When user click that button, it will be open SignIn Activity.
            val loginIntent = Intent(this, SignInActivity::class.java)
            startActivity(loginIntent)
        }

        // Add onClickListener to the Sign Up Button at the homepage.
        homeSignUpButton.setOnClickListener{

            // When user click that button, it will be open SignUp Activity.
            val signUpIntent = Intent(this, SignUpActivity::class.java)
            startActivity(signUpIntent)
        }
    }
}