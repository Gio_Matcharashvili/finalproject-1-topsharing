package com.example.topsharing

import android.content.Intent
import android.nfc.Tag
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlin.math.max

class SignUpActivity : AppCompatActivity() {

    // They are for Firebase Auth and database.
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        // Call the main function.
        init()
    }

    // Create main function.
    private fun init(){
        // This is for Firebase Auth.
        auth = Firebase.auth
        database = Firebase.database.reference

        // Set onClick listener to the Sign Up button.
        upSignUpButton.setOnClickListener{
            // Call the SignUp function.
            signUp()
        }
    }

    // Create function where we check credentials.
    private fun sendInfo(name:String,email:String,password: String){

        // Connect firebase realtime database.
        val database = Firebase.database
        val myRef = database.getReference("users")

        // Take information only once.
        myRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                // In the base we have all the user information
                var base: HashMap<String, HashMap<String,String>>? = dataSnapshot
                    .getValue<HashMap<String,HashMap<String,String>>>()

                // keys are usernames
                val keys = base?.keys

                // Create emails list where we put all the emails.
                val emails:MutableSet<String> = mutableSetOf()

                // Put all the emails in the emails list.
                for (key in keys!!){
                    base?.get(key)?.get("email")?.let { emails.add(it) }
                }

                // Check if username already exist, if it is show error.
                if (!keys?.contains(name)!!){
                    // Check if email already exist, if it is show error.
                    if (!(email in emails)){
                        // Create info hashmap where we put email and avatar information.
                        var info:HashMap<String,String> = hashMapOf()

                        //Put this information.
                        info.put("email",email)
                        info.put("avatar","default")

                        // now put info into the base.
                        base?.put(name,info)

                        // check if base is null
                        if (base != null) {
                            // If it is not, call sendAuth function.
                            sendAuth(email,password,base)
                        }
                    }else{
                        Toast.makeText(baseContext, "Email already exists.",
                            Toast.LENGTH_SHORT).show()
                        }

                }else{
                    Toast.makeText(baseContext, "Username already exists.",
                        Toast.LENGTH_SHORT).show()
                }

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Toast.makeText(baseContext, "Can't Connect to the server.",
                    Toast.LENGTH_SHORT).show()
            }


        })
    }

    // Create function where we send auth to the firebase server.
    private fun sendAuth(email: String,password:String,base:HashMap<String, HashMap<String,String>>){
        // Connect firebase realtime database.
        val database = Firebase.database
        val myRef = database.getReference("users")

        // Show progressbar while user waits.
        upProgressBar.visibility = View.VISIBLE

        // Make sign up button not clickable while user waits.
        upSignUpButton.isClickable = false

        // Send request to the firebase.
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Toast.makeText(baseContext, "Authentication succeed!",
                        Toast.LENGTH_SHORT).show()
                    val user = auth.currentUser

                    // When everything succeed it we set data to the realtime database.
                    myRef.setValue(base)

                    // If everything goes successfully, open home page.
                    val homeIntent = Intent(this, HomePageActivity::class.java)
                    startActivity(homeIntent)

                } else {
                    // If sign in fails, display a message to the user.
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }

                // Make Progress bar disappear after wait.
                upProgressBar.visibility = View.GONE

                // Make sign up button clickable after wait.
                upSignUpButton.isClickable = true
            }

    }

    // Create SignUp function.
    private fun signUp(){

        // Take all the credentials.
        val username = upUsernameEditText.text.toString()
        val email = upEmailEditText.text.toString()
        val password = upPasswordEditText.text.toString()
        val confirmPassword = upConfirmPasswordEditText.text.toString()
        val database = Firebase.database
        val myRef = database.getReference("users")

        // If credentials are not empty continues, else show error.
        if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty() && confirmPassword.isNotEmpty()){
            // If spaces is not in the credentials continues, else show error.
            if (" " !in username && " " !in email && " " !in password && " " !in confirmPassword){
                // Check if username length is less than 11.
                if (username.length < 11){
                    // Check if username length is more than 3.
                    if (username.length > 3){
                        // Check if password length is less than 9.
                        if (password.length < 9){
                            // Check if password length is more than 3.
                            if (password.length > 3){
                                // Check if password and confirm password is the same.
                                if (password == confirmPassword){
                                    // Check if '@gmail.com' is in the email.
                                    if ("@gmail.com" in email){

                                        // send information to check validation.
                                        sendInfo(username,email,password)

                                    }else{
                                        Toast.makeText(this, "Use Gmail email with '@gmail.com'", Toast.LENGTH_SHORT).show()
                                    }
                                }else{
                                    Toast.makeText(this, "Password and confirm password is not same.", Toast.LENGTH_SHORT).show()
                                }
                            }else{
                                Toast.makeText(this, "Password length must be more than 3.", Toast.LENGTH_SHORT).show()
                            }
                        }else{
                            Toast.makeText(this, "Password length must be less than 9.", Toast.LENGTH_SHORT).show()
                        }
                    }else{
                        Toast.makeText(this, "Username length must be more than 3.", Toast.LENGTH_SHORT).show()
                    }
                }else{
                    Toast.makeText(this, "Username length must be less than 11.", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this, "You can't use spaces.", Toast.LENGTH_SHORT).show()
            }
        }else{
            Toast.makeText(this, "Please fill all graphs.", Toast.LENGTH_SHORT).show()
        }
    }

}