# Introduction 
 TopSharing is the social network application, where you can write your own and see other users' posts.
 
# Features
###### 1) sign up
You can create your own account with username, email and password. all the usernames are different.
	
###### 2) sign in
You can login into your account and see your own or another users's posts.

###### 3) write your own post.
Users can write their posts and another users can see it.

###### 4) avatars
Users have their avatars, which can be switchable into the future.

# Getting started

###### 1) download TopSharing application.

###### 2) click sign up button.

###### 3) create your account and start using it.

# Contacts
###### If you have problems, questions, ideas or suggestions, please contact us:

Application Developer: giorgi.matcharashvili.1@btu.edu.ge

Designer: gigi.giorgadze.1@btu.edu.ge